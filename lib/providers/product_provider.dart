import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_shop_some/api.dart';
import 'package:flutter_shop_some/models/product.dart';


final productProvider = FutureProvider((ref) => ProductProvider().getData());

class ProductProvider {

   Future<List<Product>> getData ()async{
    final dio = Dio();
     try{
      final response = await dio.get(Api.baseUrl);
      final data =  (response.data as List).map((e) =>  Product.fromJson(e)).toList();
      return data;

     }on DioError catch (e){
       print(e);
       throw e;
     }
  }




}