import 'package:flutter/material.dart';
import 'package:flutter_shop_some/models/users.dart';
import 'package:flutter_shop_some/screens/auth_Screen.dart';
import 'package:flutter_shop_some/screens/home_screen.dart';
import 'package:hive_flutter/hive_flutter.dart';




class StatusCheck extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<User>>(
        valueListenable: Hive.box<User>('users').listenable(),
        builder: (context, box, child){
          final user = box.values.toList();
          return user.length == 0 ? AuthScreen() : HomeScreen(user[0]);
        }
    );
  }
}
