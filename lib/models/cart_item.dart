import 'package:hive/hive.dart';
part 'cart_item.g.dart';


@HiveType(typeId: 1)
class CartItem extends HiveObject{
  @HiveField(0)
  late String id;

  @HiveField(1)
  late String title;

  @HiveField(2)
  late String quantity;

  @HiveField(3)
  late String imageUrl;

  @HiveField(4)
  late String  total;


  CartItem({
    required this.title,
    required this.id,
    required this.quantity,
    required this.total,
    required this.imageUrl
});


}