import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_shop_some/models/cart_item.dart';
import 'package:flutter_shop_some/models/users.dart';
import 'package:flutter_shop_some/screens/status_screen.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';



final boxA = Provider<List<CartItem>>((ref) => []);


void main () async{
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.black
    )
  );
  WidgetsFlutterBinding.ensureInitialized();
  Directory directory = await getApplicationDocumentsDirectory();
  Hive.init(directory.path);
  Hive.registerAdapter(UserAdapter());
  Hive.registerAdapter(CartItemAdapter());
  await Hive.openBox<User>('users');
final cart_hive =  await Hive.openBox<CartItem>('carts');
  runApp(
      ProviderScope(
      overrides: [
        boxA.overrideWithValue(cart_hive.values.toList())
      ],
      child: Home()));
}



class Home extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
        home: StatusCheck()
    );
  }
}
